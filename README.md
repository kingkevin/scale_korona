This project is about interfacing a bathroom weight scale with the computer.
The goal is to be able to connect the scale to the computer and have it sent the weight it measures.

The scale used is a Korona KFW-55.
This scale was simply around when the project started.

To connect the scale to the computer a Arduino Nano development board is used.
The Atmel ATmega 328P micro-controller is integrated in the scale case.
It reads the PWM signal, interprets the weight, and sends it on UART (over USB).

The scale has a JP1 header on the board.
Pin 1 is the one near the switch.
The pinout is the following:
1. ground
2. low
3. high on power on
4. high on power on
5. PWM
6. high on power on

The scale's IC provides a Pulse Width Modulated (PWM) signal.
This encodes the weight measures.
It's frequency is around 3 Hz (it's not exact).
The high and low duty cycle encode the current weight measured.
The high duty cycle decreases when the weight increases.
The low duty cycle increases when the weight increases.
The low duty cycle is linearly proportional to the weight.
It still needs a minimum weight to start measuring correctly.
The origin of this linear curve is not at 0.

The micro-controller code is written in C.
It reads the state of the scale using pin 3.
It reads the weight on the scale using pin 5.
By programming the curve parameters in the code it can calculate the weight.
These parameters have been experimentally identify, using the measured values in `scale.ods`.
