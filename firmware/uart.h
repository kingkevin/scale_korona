/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This is part of the washkarte auflader program.
 * It handles the USART configuration
 */

#define BAUD_TOL 3 /* set tolerance to 3% to allow 115200 baudrate with 16MHz clock, else use 9600 for default <2% */
#define BAUD 115200 /* serial baudrate */

FILE uart_output;
FILE uart_input;
FILE uart_io;

void uart_init(void);
void uart_putchar(char c, FILE *stream);
char uart_getchar(FILE *stream);

