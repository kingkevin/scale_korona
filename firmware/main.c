/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <stdint.h> // Standard Integer Types
#include <stdio.h> // Standard IO facilities
#include <stdlib.h> // General utilities
#include <stdbool.h> // Boolean
#include <string.h> // Strings

#include <avr/io.h> // AVR device-specific IO definitions
#include <util/delay.h> // Convenience functions for busy-wait delay loops
#include <avr/interrupt.h> // Interrupts
#include <avr/wdt.h> // Watchdog timer handling
#include <avr/pgmspace.h> // Program Space Utilities
#include <avr/sleep.h> // Power Management and Sleep Modes

#include "uart.h" // basic UART functions
#include "main.h" // main definitions

/* contants */
// max measurement jitter for high and low value 8/3
#define JITTER_HIGH 11
#define JITTER_LOW 5
// how many value have to be stable before showing output
#define MEASUREMENT_STABLE 3

/* variables */
volatile uint8_t state_portD; // the state of port D
volatile bool scale_on = false; // is the scale on (based on the SCALE_ON PIN)
volatile bool scale_old = false; // the previous state (to detect changes)
volatile bool pwm_high = false; // is the PWM for the weight measurement high
volatile bool measurement_flag = false; // is a PWM  measurement value ready
volatile uint16_t measurement_value = 0; // the PWM measurement value from the timer

/* switch off LED */
void led_off(void)
{
	PORTB &= ~(1<<LED); // remove power to LED
}

/* switch on LED */
void led_on(void)
{
	PORTB |= (1<<LED); // provide power to LED
}

/* toggle LED */
void led_toggle(void)
{
	PINB |= (1<<LED);
}

/* scale on interrupt
 * PCI2 Interrupt Vector for PCINT[23:16]/PORTD
 */
ISR(PCINT2_vect)
{
	if ((state_portD&(1<<SCALE_ON))!=(PIND&(1<<SCALE_ON))) { // scale on state changed
		if (PIND&(1<<SCALE_ON)) {
			scale_on = true;
			led_on();
		} else {
			scale_on = false;
			led_off();
		}
	}
	state_portD = PIND; // save new state
}

/* PWM weight signal
 * analog comparator interrupt
 */
ISR(ANALOG_COMP_vect)
{
	if (scale_on && !measurement_flag) { // only save value if scale is on and previous value is read
		measurement_value = TCNT1; // save value
		TCNT1 = 0; // reset timer		
		if (ACSR&(1<<ACO)) { // PWM raising edge
			pwm_high = false;
			led_on();
		} else { // PWM falling edge
			pwm_high = true;
			led_off();
		}
		measurement_flag = true; // signal new value is ready
	}
}

/* disable watchdog when booting */
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));
void wdt_init(void)
{
	MCUSR = 0;
	wdt_disable();

	return;
}

/* initialize GPIO */
void io_init(void)
{
	/* use UART as terminal */
	uart_init();
	stdout = &uart_output;
	stdin  = &uart_input;

	/* gpio */
	/* LED */
	DDRB |= (1<<LED); // LED is driven by pin (set as output)
	led_off();

	/* scale on signal */
	DDRD &= ~(1<<SCALE_ON); // SIGNAL_ON is input (should be per default)
	PCIFR &= ~(1<<PCIF2); // clear interrupt flag for SCALE_ON on PCINT[23:16]/PORTD
	PCICR |= (1<<PCIE2); // enable interrupt for PCINT[23:16]/PORTD
	PCMSK2 |= (1<<PCINT21); // enable interrupt for SCALE_ON
	state_portD = PIND; // save state to detect change

	/* PWM scale weight signal on analog comparator */
	DDRD &= ~((1<<SCALE_PWM)|(1<<REF_PWM)); // analog comparator is input (should be per default)
	ADCSRA &= ~(1<<ADEN); // switch off ADC
	ADCSRB &= ~(1<<ACME); // disable analog comparator multiplexer, use AIN1 as negative input (should be per default)
	ACSR &= ~(1<<ACD); // enable analog comparator (should be per default)
	ACSR &= ~(1<<ACBG); // use AIN0 as positiv input (should be per default)
	ACSR |= (1<<ACI); // clear analog comparator interrup flag
	ACSR &= ~((1<<ACIS1)|(1<<ACIS0)); // comparator interrupt on output toggle
	DIDR1 |= ((1<<AIN1D)|(1<<AIN0D)); // disable digital input buffer on AIN0 and AIN1
	ACSR |= (1<<ACIE); // enable analog comparator interrupt

	/* use timer 1 (with capture unit) to measure PWM */
	TCCR1A &= ~((1<<WGM11)|(1<<WGM10)); // mode 0: normal (should be per default)
	TCCR1B &= ~((1<<WGM13)|(1<<WGM12)); // mode 0: normal (should be per default)
	TCCR1B |= (1<<CS12)|(0<<CS11)|(0<<CS10); // set prescale to 256 for 1.048576s before overflow (PWM should be 3Hz)

	sei(); /* enable interrupts */
}

int main(void)
{
	uint16_t measurement_high = 0, measurement_low = 0; // the average measurement value
	uint16_t zero_high = 0, zero_low = 0; // the intial zero value
	uint8_t count_high = 0, count_low = 0; // the number of stable counts
	
	io_init(); // initialize IOs

	printf(PSTR("welcome to the body scale weight reader\n")); // print welcome message
	while (true) { // endless loop for micro-controller
		/* display if scale switches on */
		if (scale_old!=scale_on && scale_on) {
#ifdef DEBUG
			printf(PSTR("scale start\n"));
#endif
			measurement_high = measurement_low = zero_high = zero_low = count_high = count_low = 0; // re-initialize values
			scale_old = scale_on; // save new state
		}
		/* display value if available */
		if (measurement_flag) {
			if (pwm_high) {
#ifdef DEBUG
				printf("high: %u\n", measurement_value);
#endif
				if (measurement_value<=measurement_high+JITTER_HIGH && measurement_value>=measurement_high-JITTER_HIGH) { // stable value
					measurement_high = (measurement_high+measurement_value)/2;
					count_high += 1;
				} else {
					measurement_high = measurement_value;
					count_high = 0;
				}
			} else {
#ifdef DEBUG
				printf("low: %u\n", measurement_value);
#endif
				if (measurement_value<=measurement_low+JITTER_LOW && measurement_value>=measurement_low-JITTER_LOW) { // stable value
					measurement_low = (measurement_low+measurement_value)/2;
					count_low += 1;
				} else {
					measurement_low = measurement_value;
					count_low = 0;
				}
			}
#ifdef DEBUG
			printf("values high/low (stable): %u (%u)/%u (%u)\n", measurement_high, count_high, measurement_low, count_low);
#endif
			measurement_flag = false;
		}
		/* display weight if values are stable */
		if (count_high>=MEASUREMENT_STABLE && count_low>=MEASUREMENT_STABLE) {
			if (zero_high == 0 && zero_low == 0) { // initialize zero values
				zero_high = measurement_high;
				zero_low = measurement_low;
#ifdef DEBUG
				printf("zero values high low: %u %u\n", zero_high, zero_low);
#endif
				count_high = count_low = 0;
			} else { // print measurement weight
#ifdef DEBUG
				printf("%u %u 0\n", zero_high, zero_low);
				printf("%u %u t1\n", measurement_high, measurement_low);
#endif
	
				// the low measurement are linear and more stable
				// normally it should not have a origin, but the linear estimation shows one and it works better
				const double orig_low = -1.92285;
				const double coef_low = 0.0258119;
				double weight_low = (int16_t)(measurement_low-zero_low)*coef_low+orig_low;
				if (weight_low<orig_low*-1.5 && weight_low>orig_low*1.5) {
					weight_low = 0;
				}
				printf("%.2f kg\n", weight_low);
			}
			count_high = count_low = 0;
		}
		/* display if scale switches off */
		if (scale_old!=scale_on && !scale_on) {
#ifdef DEBUG
			printf(PSTR("scale stop\n")); // actually measurement is finished and the on switch is released
#endif
			scale_old = scale_on; // save new state
		}
		/* go to sleep and wait for next interrupt */
		set_sleep_mode(SLEEP_MODE_IDLE);
		sleep_mode();
	}
	return 0;
}

