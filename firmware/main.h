/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This is the main part of the waschkarte auflader program.
 */

/* peripherals */
/* LED to indicate scale reading
 * pin: PB5, LED L
 */
#define LED PB5
/* high when the scale is on
 * pin: PD5, PCINT21, D5
 */
#define SCALE_ON PD5
/* PWM weight signal
 * PWM frequency is ~3Hz
 * high time increases with increasing weight (propotionnally)
 * low time decreases with increasing weight
 * signal level is 1V
 * positive pin of analog comparator used to measure PWM
 * pin: PD6, AIN0, D6
*/
#define SCALE_PWM PD6
/* negative pin of analog comparator
 * set at 0.2-0.8V using voltage divider
 * used to detect PWM signal change
 * pin: PD7, AIN1, D7
 */
#define REF_PWM PD7

